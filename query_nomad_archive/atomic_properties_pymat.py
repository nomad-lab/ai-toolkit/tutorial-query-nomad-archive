#!/usr/bin/env python
# coding: utf-8

# In[1]:


from query_nomad_archive.metainfo import atomic_properties_pymat,metadata
import pandas as pd
import numpy as np
import urllib, json, re, typing
from pandas.io.json import json_normalize


# 
# # Read the json file directly from pymatgen source into a Pandas dataframe

# In[2]:


#url = "https://raw.githubusercontent.com/materialsproject/pymatgen/ecdc7e40e048a3868a6579f3a9f9a41168c9bf75/pymatgen/core/periodic_table.json"


# In[3]:


# response = urllib.request.urlopen(url)
# data = json.loads(response.read())
# df1 = pd.DataFrame(data)


# ### Flatten the nested dictionary of Shannon radii feature into simple key-value dictionary

# In[4]:


df1 = pd.read_json('./data/json/periodic_table.json')


# In[5]:


for elem in df1.columns:
    if type(df1.loc['Shannon radii'][elem]) == dict:
        df1.loc['Shannon radii'][elem] = (json_normalize(df1.loc['Shannon radii'][elem]).to_dict(orient='records')[0])


# ### replace the missing spin data for Shannon radii keys into a dictionary

# In[6]:


repl_dict ={}
for elem in df1.columns:
    if type(df1.loc['Shannon radii'][elem]) == dict:
        try:
            df1.loc['Shannon radii'][elem].keys()
            for key in df1.loc['Shannon radii'][elem].keys():
                if re.search(r"(.High Spin.)", key):
                    new_key = re.sub(r"(.High Spin.)",'.high_spin.',key)
                    #print(key)
                elif re.search(r"(.Low Spin.)", key):
                    new_key = re.sub(r"(.Low Spin.)",'.low_spin.',key)
                    #print(key)
                else: 
                    new_key = re.sub(r"(\.\.)",'.no_spin.',key)

                    #new_key = re.sub(r"(\.\.)",'.No_Spin.',key)
                repl_dict.update({key : new_key})
        except AttributeError:
            continue
#This rename_keys() function is called to replace keys of each element            
def rename_keys(d, keys):
    d = dict([(keys.get(k), v) for k, v in d.items()])
    return d

for elem in df1.columns:
    if type(df1.loc['Shannon radii'][elem]) == dict:
        test = rename_keys(df1.loc['Shannon radii'][elem], repl_dict)
        df1.loc['Shannon radii'][elem] = test
        


# ### Dataframe manulpulation to sort the data from lowest to highest atomic number

# In[7]:


df2 = df1.transpose()
df2.reset_index();
df3 = df2.sort_values('Atomic no')
df3.reset_index();


# ### Remove the unwanted text and units from dataframe columns (Cleaning the dataset)

# In[8]:



df3['Atomic radius'] = np.where(df3['Atomic radius'] == 'no data',np.nan, df3['Atomic radius'])
df3['Atomic orbitals'] = np.where(df3['Atomic orbitals'] == 'no data',np.nan, df3['Atomic orbitals'])
df3['Atomic radius calculated'] = np.where(df3['Atomic radius calculated'] == 'no data',np.nan, df3['Atomic radius calculated'])
df3['Mineral hardness'] = np.where(df3['Mineral hardness'] == 'no data',np.nan, df3['Mineral hardness'])
df3['Poissons ratio'] = np.where(df3['Poissons ratio'] == 'no data',np.nan, df3['Poissons ratio'])
df3['Metallic radius'] = np.where(df3['Metallic radius'] == 'no data',np.nan, df3['Metallic radius'])

df3['Coefficient of linear thermal expansion'] = df3['Coefficient of linear thermal expansion'].str.replace(r"(?:no data)|(?: x10<sup>-6</sup>K<sup>-1</sup>)", '')
df3['Boiling point'] = df3['Boiling point'].str.replace(r"(?: K)|(?:no data)", '')
df3['Brinell hardness'] = df3['Brinell hardness'].str.replace(r"(?:no data)|(?: MN m<sup>-2</sup>)", '')
df3['Bulk modulus'] = df3['Bulk modulus'].str.replace(r"(?: GPa)|(?:no data)|(?:\([^()]*\)GPa)", '')
df3['Youngs modulus'] = df3['Youngs modulus'].str.replace(r"(?: GPa)|(?:no data)", '')
df3['Critical temperature'] = df3['Critical temperature'].str.replace(r"(?:K)|(?:no data)", '')
df3['Density of solid'] = df3['Density of solid'].str.replace(r"(?:no data)|(?: kg m<sup>-3</sup>)", '')
df3['Electrical resistivity'] = df3['Electrical resistivity'].str.replace(r"(?:no data)|(?: 10<sup>-8</sup> &Omega; m)|(?:; 10<sup>15</sup>)|(?:; 10<sup>12</sup>)|(?:; 10<sup>18</sup>)|(?:; 10<sup>23</sup>)|(?:; 10<sup>10</sup>)|(?:&gt10<sup>-8</sup> &Omega; m)|(?:10<sup>-8</sup> &Omega; m)", '')
df3['Liquid range'] = df3['Liquid range'].str.replace(r"(?: K)|(?:no data)", '')
df3['Melting point'] = df3['Melting point'].str.replace(r"(?: K)|(?:no data)", '')
df3['Molar volume'] = df3['Molar volume'].str.replace(r"(?:no data)|(?: cm<sup>3</sup>)", '')


df3['Reflectivity'] = df3['Reflectivity'].str.replace(r"(?:no data)|(?:%)", '')
df3['Rigidity modulus'] = df3['Rigidity modulus'].str.replace(r"(?: GPa)|(?:no data)", '')
df3['Superconduction temperature'] = df3['Superconduction temperature'].str.replace(r"(?: K)|(?:no data)", '')
df3['Thermal conductivity'] = df3['Thermal conductivity'].str.replace(r"(?:no data)|(?: W m<sup>-1</sup> K<sup>-1</sup>)|(?:W m<sup>-1</sup> K<sup>-1</sup>)", '')
df3['Velocity of sound'] = df3['Velocity of sound'].str.replace(r"(?:no data)|(?: m s<sup>-1</sup>)", '')
df3['Vickers hardness'] = df3['Vickers hardness'].str.replace(r"(?:no data)|(?: MN m<sup>-2</sup>)", '')
df3['Van der waals radius'] = df3['Van der waals radius'].str.replace(r"(?:no data)", '')
df3['Electronic structure'] = df3['Electronic structure'].str.replace(r"(?:<sup>)|(?:<\/sup>)|(?:\([^()]*\))", '')



# In[9]:


df4 = df3.reset_index()
df4.rename(columns={"index": "Element Symbol"},inplace = True)
df4.drop('iupac_ordering', axis=1, inplace=True)


# ### Lastly fill the empty features with np.nan values

# In[10]:


df_clean = df4.replace(r'^\s*$', np.nan, regex=True)


# In[11]:


df_clean;


# # Section instantiation and its objects attributes are filled. This will create 118 objects unique ( atomic element symbol) which can be used to access its properties  

# In[12]:


objs = []
for i in df_clean['Element Symbol']:
    pymat = atomic_properties_pymat()
    prop = pymat.m_create(metadata)
    objs.append(pymat)


# In[13]:


count = 0
for obj in objs:
        obj.section_pymat_metadata.source = 'Pymatgen'
        obj.section_pymat_metadata.atomic_spin_setting = 'NA'
        obj.atomic_number = int(df_clean.iloc[count,df_clean.columns.get_loc("Atomic no")])
        obj.atomic_element_symbol = str(df_clean.iloc[count,df_clean.columns.get_loc("Element Symbol")])
        obj.section_pymat_metadata.atomic_method = 'NA'
        obj.section_pymat_metadata.atomic_basis_set = 'NA'
        obj.atomic_mass = float(df_clean.iloc[count,df_clean.columns.get_loc("Atomic mass")])
        obj.atomic_radius = float(df_clean.iloc[count,df_clean.columns.get_loc("Atomic radius")])
        obj.atomic_radius_calculated = float(df_clean.iloc[count,df_clean.columns.get_loc("Atomic radius calculated")])
        obj.atomic_orbitals = df_clean.iloc[count,df_clean.columns.get_loc("Atomic orbitals")]
        obj.boiling_point = float(df_clean.iloc[count,df_clean.columns.get_loc("Boiling point")])
        obj.brinell_hardness = float(df_clean.iloc[count,df_clean.columns.get_loc("Brinell hardness")])
        obj.bulk_modulus = df_clean.iloc[count,df_clean.columns.get_loc("Bulk modulus")]
        obj.coeff_olte = float(df_clean.iloc[count,df_clean.columns.get_loc("Coefficient of linear thermal expansion")])
        obj.common_ox_states = df_clean.iloc[count,df_clean.columns.get_loc("Common oxidation states")]
        obj.critical_temperature = float(df_clean.iloc[count,df_clean.columns.get_loc("Critical temperature")])
        obj.density_of_solid = float(df_clean.iloc[count,df_clean.columns.get_loc("Density of solid")])
        obj.electrical_resistivity = df_clean.iloc[count,df_clean.columns.get_loc("Electrical resistivity")]
        obj.electronic_structure = df_clean.iloc[count,df_clean.columns.get_loc("Electronic structure")]
        obj.ionic_radii = df_clean.iloc[count,df_clean.columns.get_loc("Ionic radii")]
        obj.liquid_range = float(df_clean.iloc[count,df_clean.columns.get_loc("Liquid range")])
        obj.melting_point = df_clean.iloc[count,df_clean.columns.get_loc("Melting point")]
        obj.mendeleev_no = float(df_clean.iloc[count,df_clean.columns.get_loc("Mendeleev no")])
        obj.mineral_hardness = df_clean.iloc[count,df_clean.columns.get_loc("Mineral hardness")]
        obj.molar_volume = float(df_clean.iloc[count,df_clean.columns.get_loc("Molar volume")])
        obj.atomic_element_name = str(df_clean.iloc[count,df_clean.columns.get_loc("Name")])
        obj.oxidation_states = df_clean.iloc[count,df_clean.columns.get_loc("Oxidation states")]
        obj.poisson_ratio = float(df_clean.iloc[count,df_clean.columns.get_loc("Poissons ratio")])
        obj.atomic_refelctivity = float(df_clean.iloc[count,df_clean.columns.get_loc("Reflectivity")])
        obj.atomic_refractive_index = df_clean.iloc[count,df_clean.columns.get_loc("Refractive index")]
        obj.rigidity_modulus = float(df_clean.iloc[count,df_clean.columns.get_loc("Rigidity modulus")])
        obj.shannon_radii = df_clean.iloc[count,df_clean.columns.get_loc("Shannon radii")]
        obj.thermal_cond = df_clean.iloc[count,df_clean.columns.get_loc("Thermal conductivity")]
        obj.van_der_waals_rad = float(df_clean.iloc[count,df_clean.columns.get_loc("Van der waals radius")])
        obj.velocity_of_sound = float(df_clean.iloc[count,df_clean.columns.get_loc("Velocity of sound")])
        obj.vickers_hardness = float(df_clean.iloc[count,df_clean.columns.get_loc("Vickers hardness")])
        obj.x = float(df_clean.iloc[count,df_clean.columns.get_loc("X")])
        obj.youngs_modulus = float(df_clean.iloc[count,df_clean.columns.get_loc("Youngs modulus")])
        obj.metallic_radius = float(df_clean.iloc[count,df_clean.columns.get_loc("Metallic radius")])
        obj.iupac_ordering = float(df_clean.iloc[count,df_clean.columns.get_loc("IUPAC ordering")])
        obj.icsd_oxd_states = df_clean.iloc[count,df_clean.columns.get_loc("ICSD oxidation states")]
        obj.supercond_temp = df_clean.iloc[count,df_clean.columns.get_loc("Superconduction temperature")]
        obj.nmr_quadrapole_mom = df_clean.iloc[count,df_clean.columns.get_loc("NMR Quadrupole Moment")]
        obj.max_oxd_state = float(df_clean.iloc[count,df_clean.columns.get_loc("Max oxidation state")])
        obj.min_oxd_state = float(df_clean.iloc[count,df_clean.columns.get_loc("Min oxidation state")])
        obj.ionic_radii_hs = df_clean.iloc[count,df_clean.columns.get_loc("Ionic radii hs")]
        obj.ionic_radii_ls = df_clean.iloc[count,df_clean.columns.get_loc("Ionic radii ls")]
        count+=1
        
atomic_element_symbol = df_clean['Element Symbol'].values
for count, symbol in enumerate(atomic_element_symbol):
    globals()[symbol] = objs[count]


# In[14]:


def definition(abc):
    if abc == 'atomic_number':
        return atomic_properties_pymat.atomic_number.__doc__
    if abc == 'atomic_element_symbol':
        return atomic_properties_pymat.atomic_element_symbol.__doc__
    if abc == 'atomic_mass':
        return atomic_properties_pymat.atomic_mass.__doc__
    if abc == 'atomic_radius':
        return atomic_properties_pymat.atomic_radius.__doc__
    if abc == 'atomic_radius_calculated':
        return atomic_properties_pymat.atomic_radius_calculated.__doc__
    if abc == 'atomic_orbitals':
        return atomic_properties_pymat.atomic_orbitals.__doc__
    if abc == 'boiling_point':
        return atomic_properties_pymat.boiling_point.__doc__
    if abc == 'brinell_hardness':
        return atomic_properties_pymat.brinell_hardness.__doc__
    if abc == 'coeff_olte':
        return atomic_properties_pymat.coeff_olte.__doc__
    if abc == 'common_ox_states':
        return atomic_properties_pymat.common_ox_states.__doc__
    if abc == 'critical_temperature':
        return atomic_properties_pymat.critical_temperature.__doc__
    if abc == 'density_of_solid':
        return atomic_properties_pymat.density_of_solid.__doc__
    if abc == 'electrical_resistivity':
        return atomic_properties_pymat.electrical_resistivity.__doc__
    if abc == 'ionic_radii':
        return atomic_properties_pymat.ionic_radii.__doc__
    if abc == 'liquid_range':
        return atomic_properties_pymat.liquid_range.__doc__
    if abc == 'melting_point':
        return atomic_properties_pymat.melting_point.__doc__
    if abc == 'mendeleev_no':
        return atomic_properties_pymat.mendeleev_no.__doc__
    if abc == 'mineral_hardness':
        return atomic_properties_pymat.mineral_hardness.__doc__
    if abc == 'molar_volume':
        return atomic_properties_pymat.molar_volume.__doc__
    if abc == 'atomic_element_name':
        return atomic_properties_pymat.atomic_element_name.__doc__
    if abc == 'oxidation_states':
        return atomic_properties_pymat.oxidation_states.__doc__
    if abc == 'poisson_ratio':
        return atomic_properties_pymat.poisson_ratio.__doc__
    if abc == 'atomic_refelctivity':
        return atomic_properties_pymat.atomic_refelctivity.__doc__
    if abc == 'atomic_refractive_index':
        return atomic_properties_pymat.atomic_refractive_index.__doc__
    if abc == 'rigidity_modulus':
        return atomic_properties_pymat.rigidity_modulus.__doc__
    if abc == 'shannon_radii':
        return atomic_properties_pymat.shannon_radii.__doc__
    if abc == 'supercond_temp':
        return atomic_properties_pymat.supercond_temp.__doc__
    if abc == 'thermal_cond':
        return atomic_properties_pymat.thermal_cond.__doc__
    if abc == 'van_der_waals_rad':
        return atomic_properties_pymat.van_der_waals_rad.__doc__
    if abc == 'velocity_of_sound':
        return atomic_properties_pymat.velocity_of_sound.__doc__
    if abc == 'vickers_hardness':
        return atomic_properties_pymat.vickers_hardness.__doc__
    if abc == 'x':
        return atomic_properties_pymat.x.__doc__
    if abc == 'youngs_modulus':
        return atomic_properties_pymat.youngs_modulus.__doc__
    if abc == 'metallic_radius':
        return atomic_properties_pymat.metallic_radius.__doc__
    if abc == 'iupac_ordering':
        return atomic_properties_pymat.iupac_ordering.__doc__
    if abc == 'icsd_oxd_states':
        return atomic_properties_pymat.icsd_oxd_states.__doc__
    if abc == 'nmr_quadrapole_mom':
        return atomic_properties_pymat.nmr_quadrapole_mom.__doc__
    if abc == 'max_oxd_state':
        return atomic_properties_pymat.max_oxd_state.__doc__
    if abc == 'min_oxd_state':
        return atomic_properties_pymat.min_oxd_state.__doc__
    if abc == 'ionic_radii_hs':
        return atomic_properties_pymat.ionic_radii_hs.__doc__
    if abc == 'ionic_radii_ls':
        return atomic_properties_pymat.ionic_radii_ls.__doc__
    if abc == 'source':
        return metadata.source.__doc__
    if abc == 'atomic_spin_setting':
        return metadata.atomic_spin_setting.__doc__
    if abc == 'atomic_method':
        return metadata.atomic_method.__doc__
    if abc == 'atomic_basis_set':
        return metadata.atomic_basis_set.__doc__


# In[15]:


def symbol(abc):
    return globals()[abc]


# In[16]:


Co.shannon_radii


# In[ ]:




