#!/usr/bin/env python
# coding: utf-8

# In[1]:


from bokeh.io import output_notebook, output_file,show, push_notebook
from bokeh.models import ColumnDataSource,CustomJS, HoverTool,ColorBar, NumeralTickFormatter,LinearColorMapper,Select
from bokeh.plotting import figure
from bokeh.sampledata.periodic_table import elements
from bokeh.transform import dodge, factor_cmap, linear_cmap
from bokeh.palettes import PRGn, RdYlGn,Blues8,Colorblind
from bokeh.layouts import column, layout,row
from ipywidgets import interact, Layout
import pandas as pd


# ## using already available dataframe in bokeh package element, modify it contents as per our need

# In[2]:


# Use periods as y axis and groups as x axis for our plot
periods = ["I", "II", "III", "IV", "V", "VI", "VII"," ","LC","AC"]
groups = [str(x) for x in range(1, 19)]
pd.options.mode.chained_assignment = None

# In[3]:


def clean():
    df = elements.copy()
    df["atomic mass"] = df["atomic mass"].astype(str)
    df["group"] = df["group"].astype(str)
    df["period"] = [periods[x-1] for x in df.period]
    df1 = df[df.group != "-"]


    df = elements.copy()
    df["atomic mass"] = df["atomic mass"].astype(str)
    df["group"] = df["group"].astype(str)
    df["period"] = df["period"].astype(str)
    df["period"].replace(to_replace ="6",value ="LC",inplace = True)
    df["period"].replace(to_replace ="7",value ="AC", inplace = True)
    df2 = df[df.group == "-"]
    a = [*range(4,18,1),*range(4,18,1)]
    for count, obj in enumerate(a):
        df2.iloc[count,df2.columns.get_loc("group")] = str(obj)
    
    new = [df1, df2]
    comb = pd.concat(new)
    df3 = comb.drop(columns=['electronegativity', 'atomic radius','ion radius','van der Waals radius','IE-1','EA','standard state','bonding type','melting point','boiling point','density','year discovered'],inplace=False)
    df3['atomic mass'] = (df3['atomic mass'].str.replace(r"(\[)|(\])", '')).astype(float)
    globals()['df3'] = df3


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[4]:


def featuref():    
    all_features = list(dffa.columns)
    remove = ['symbol','name','CPK','electronic configuration','metal','group','period']
    globals()['Feature'] = []
    for element in all_features:
        if element not in remove:
            Feature.append(element)
    return Feature

def featuret():    
    all_features = list(dftr.columns)
    remove = ['symbol','name','CPK','electronic configuration','metal','group','period']
    globals()['Feature'] = []
    for element in all_features:
        if element not in remove:
            Feature.append(element)
    return Feature


# In[5]:


def ptableplotgent(Feature):
    source = ColumnDataSource(dftr)
  
    p2 = figure(plot_width=950, plot_height=500,title="Periodic Table",
           x_range=groups, y_range=list(reversed(periods)), toolbar_location=None,tools="hover")
    
    
    if Feature == 'atomic_ea' :
        color_mapper = linear_cmap(field_name = 'atomic_ea', palette = Colorblind[8], 
                                   low = dftr['atomic_ea'].min(), 
                                   high = dftr['atomic_ea'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ea", "@atomic_ea eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_ip' :
        color_mapper = linear_cmap(field_name = 'atomic_ip', palette = Colorblind[8], 
                                   low = dftr['atomic_ip'].min(), 
                                   high = dftr['atomic_ip'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ip", "@atomic_ip eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic number' :
        color_mapper = linear_cmap(field_name = 'atomic number', palette = Colorblind[8], 
                                   low = dftr['atomic number'].min(), 
                                   high = dftr['atomic number'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic number", "@{atomic number}"),
        ("Type", "@metal"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic mass' :
        color_mapper = linear_cmap(field_name = 'atomic mass', palette = Colorblind[8], 
                                   low = dftr['atomic mass'].min(), 
                                   high = dftr['atomic mass'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic mass", "@{atomic mass} amu"),
        ("Type", "@metal")
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_hfomo' :
        color_mapper = linear_cmap(field_name = 'atomic_hfomo', palette = Colorblind[8], 
                                   low = dftr['atomic_hfomo'].min(), 
                                   high = dftr['atomic_hfomo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hfomo", "@atomic_hfomo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
    
    if Feature == 'atomic_hpomo' :
        color_mapper = linear_cmap(field_name = 'atomic_hpomo', palette = Colorblind[8], 
                                   low = dftr['atomic_hpomo'].min(), 
                                   high = dftr['atomic_hpomo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hpomo", "@atomic_hpomo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_lfumo' :
        color_mapper = linear_cmap(field_name = 'atomic_lfumo', palette = Colorblind[8], 
                                   low = dftr['atomic_lfumo'].min(), 
                                   high = dftr['atomic_lfumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lfumo", "@atomic_lfumo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_hpumo' :
        color_mapper = linear_cmap(field_name = 'atomic_hpumo', palette = Colorblind[8], 
                                   low = dftr['atomic_hpumo'].min(), 
                                   high = dftr['atomic_hpumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lpumo", "@atomic_hpumo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_lpumo' :
        color_mapper = linear_cmap(field_name = 'atomic_lpumo', palette = Colorblind[8], 
                                   low = dftr['atomic_lpumo'].min(), 
                                   high = dftr['atomic_lpumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lpumo", "@atomic_lpumo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_ip_by_half_charged_homo' :
        color_mapper = linear_cmap(field_name = 'atomic_ip_by_half_charged_homo', palette = Colorblind[8], 
                                   low = dftr['atomic_ip_by_half_charged_homo'].min(), 
                                   high = dftr['atomic_ip_by_half_charged_homo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ip_by_half_charged_homo", "@atomic_ip_by_half_charged_homo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_ea_by_half_charged_homo' :
        color_mapper = linear_cmap(field_name = 'atomic_ea_by_half_charged_homo', palette = Colorblind[8], 
                                   low = dftr['atomic_ea_by_half_charged_homo'].min(), 
                                   high = dftr['atomic_ea_by_half_charged_homo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ea_by_half_charged_homo", "@atomic_ea_by_half_charged_homo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_up_s_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_s_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_s_neg_1'].min(), 
                                   high = dftr['atomic_r_up_s_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_s_neg_1", "@atomic_r_up_s_neg_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_dn_s_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_s_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_s_neg_1'].min(), 
                                   high = dftr['atomic_r_dn_s_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_s_neg_1", "@atomic_r_dn_s_neg_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_up_p_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_p_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_p_neg_1'].min(), 
                                   high = dftr['atomic_r_up_p_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_p_neg_1", "@atomic_r_up_p_neg_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_dn_p_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_p_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_p_neg_1'].min(), 
                                   high = dftr['atomic_r_dn_p_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_p_neg_1", "@atomic_r_dn_p_neg_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_dn_d_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_d_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_d_neg_1'].min(), 
                                   high = dftr['atomic_r_dn_d_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_d_neg_1", "@atomic_r_dn_d_neg_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_up_val_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_val_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_val_neg_1'].min(), 
                                   high = dftr['atomic_r_up_val_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_val_neg_1", "@atomic_r_up_val_neg_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_dn_val_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_val_neg_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_val_neg_1'].min(), 
                                   high = dftr['atomic_r_dn_val_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_val_neg_1", "@atomic_r_dn_val_neg_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_up_s_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_s_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_s_neg_05'].min(), 
                                   high = dftr['atomic_r_up_s_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_s_neg_05", "@atomic_r_up_s_neg_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_dn_s_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_s_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_s_neg_05'].min(), 
                                   high = dftr['atomic_r_dn_s_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_s_neg_05", "@atomic_r_dn_s_neg_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_up_p_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_p_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_p_neg_05'].min(), 
                                   high = dftr['atomic_r_up_p_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_p_neg_05", "@atomic_r_up_p_neg_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_dn_p_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_p_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_p_neg_05'].min(), 
                                   high = dftr['atomic_r_dn_p_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_p_neg_05", "@atomic_r_dn_p_neg_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_up_d_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_d_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_d_neg_05'].min(), 
                                   high = dftr['atomic_r_up_d_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_d_neg_05", "@atomic_r_up_d_neg_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_dn_d_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_d_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_d_neg_05'].min(), 
                                   high = dftr['atomic_r_dn_d_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_d_neg_05", "@atomic_r_dn_d_neg_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_r_up_val_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_val_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_val_neg_05'].min(), 
                                   high = dftr['atomic_r_up_val_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_val_neg_05", "@atomic_r_up_val_neg_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_val_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_val_neg_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_val_neg_05'].min(), 
                                   high = dftr['atomic_r_dn_val_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_val_neg_05", "@atomic_r_dn_val_neg_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_up_s' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_s', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_s'].min(), 
                                   high = dftr['atomic_r_up_s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_s", "@atomic_r_up_s pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_s' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_s', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_s'].min(), 
                                   high = dftr['atomic_r_dn_s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_s", "@atomic_r_dn_s pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_up_p' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_p', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_p'].min(), 
                                   high = dftr['atomic_r_up_p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_p", "@atomic_r_up_p pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_p' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_p', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_p'].min(), 
                                   high = dftr['atomic_r_dn_p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_p", "@atomic_r_dn_p pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_up_d' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_d', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_d'].min(), 
                                   high = dftr['atomic_r_up_d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_d", "@atomic_r_up_d pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_d' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_d', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_d'].min(), 
                                   high = dftr['atomic_r_dn_d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_d", "@atomic_r_dn_d pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_up_val' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_val', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_val'].min(), 
                                   high = dftr['atomic_r_up_val'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_val", "@atomic_r_up_val pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_val' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_val', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_val'].min(), 
                                   high = dftr['atomic_r_dn_val'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_val", "@atomic_r_dn_val pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_up_s_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_s_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_s_05'].min(), 
                                   high = dftr['atomic_r_up_s_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_s_05", "@atomic_r_up_s_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_s_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_s_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_s_05'].min(), 
                                   high = dftr['atomic_r_dn_s_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_s_05", "@atomic_r_dn_s_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_up_p_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_p_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_p_05'].min(), 
                                   high = dftr['atomic_r_up_p_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_p_05", "@atomic_r_up_p_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_p_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_p_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_p_05'].min(), 
                                   high = dftr['atomic_r_dn_p_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_p_05", "@atomic_r_dn_p_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_up_d_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_d_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_d_05'].min(), 
                                   high = dftr['atomic_r_up_d_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_d_05", "@atomic_r_up_d_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_d_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_d_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_d_05'].min(), 
                                   high = dftr['atomic_r_dn_d_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_d_05", "@atomic_r_dn_d_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_up_val_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_val_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_val_05'].min(), 
                                   high = dftr['atomic_r_up_val_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_val_05", "@atomic_r_up_val_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_val_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_val_05', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_val_05'].min(), 
                                   high = dftr['atomic_r_dn_val_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_val_05", "@atomic_r_dn_val_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_up_s_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_s_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_s_1'].min(), 
                                   high = dftr['atomic_r_up_s_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_s_1", "@atomic_r_up_s_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_s_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_s_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_s_1'].min(), 
                                   high = dftr['atomic_r_dn_s_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_s_1", "@atomic_r_dn_s_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_up_p_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_p_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_p_1'].min(), 
                                   high = dftr['atomic_r_up_p_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_p_1", "@atomic_r_up_p_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_p_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_p_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_p_1'].min(), 
                                   high = dftr['atomic_r_dn_p_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_p_1", "@atomic_r_dn_p_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_up_d_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_d_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_d_1'].min(), 
                                   high = dftr['atomic_r_up_d_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_d_1", "@atomic_r_up_d_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_d_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_d_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_d_1'].min(), 
                                   high = dftr['atomic_r_dn_d_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_d_1", "@atomic_r_dn_d_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_up_val_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_up_val_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_up_val_1'].min(), 
                                   high = dftr['atomic_r_up_val_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_up_val_1", "@atomic_r_up_val_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_dn_val_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_dn_val_1', palette = Colorblind[8], 
                                   low = dftr['atomic_r_dn_val_1'].min(), 
                                   high = dftr['atomic_r_dn_val_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_dn_val_1", "@atomic_r_dn_val_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    
    p2.rect("group", "period", 0.90, 0.90, source= source, fill_alpha=0.6,color=color_mapper)
    text_props = {"source":source, "text_align": "left", "text_baseline": "middle", "text_color" : "black"}

    x1 = dodge("group", -0.4, range=p2.x_range)

    r1 = p2.text(x=x1, y="period", text="symbol", **text_props)
    r1.glyph.text_font_style="bold"

    r1 = p2.text(x=x1, y=dodge("period", 0.3, range=p2.y_range), text="atomic number", **text_props)
    r1.glyph.text_font_size="8pt"

    r1 = p2.text(x=x1, y=dodge("period", -0.35, range=p2.y_range), text="name", **text_props)
    r1.glyph.text_font_size="5pt"
    #Defines color bar
    color_bar = ColorBar(color_mapper=color_mapper['transform'],
                         formatter = NumeralTickFormatter(format='0.0000'),label_standoff = 13, width=8, location=(0,0))
    # Set color_bar location
    p2.add_layout(color_bar, 'right')
    p2.outline_line_color = None
    p2.grid.grid_line_color = None
    p2.axis.axis_line_color = None
    p2.axis.major_tick_line_color = None
    p2.axis.major_label_standoff = 0
    p2.axis.visible = False
    output_notebook(hide_banner=True)
    show(p2,notebook_handle=True)
    push_notebook()


# In[6]:


def ptableplotgenf(Feature):
    source = ColumnDataSource(dffa)
  
    p2 = figure(plot_width=950, plot_height=500,title="Periodic Table",
           x_range=groups, y_range=list(reversed(periods)), toolbar_location=None,tools="hover")
    
    
    if Feature == 'atomic_ea' :
        color_mapper = linear_cmap(field_name = 'atomic_ea', palette = Colorblind[8], 
                                   low = dffa['atomic_ea'].min(), 
                                   high = dffa['atomic_ea'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ea", "@atomic_ea eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_ip' :
        color_mapper = linear_cmap(field_name = 'atomic_ip', palette = Colorblind[8], 
                                   low = dffa['atomic_ip'].min(), 
                                   high = dffa['atomic_ip'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ip", "@atomic_ip eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic number' :
        color_mapper = linear_cmap(field_name = 'atomic number', palette = Colorblind[8], 
                                   low = dffa['atomic number'].min(), 
                                   high = dffa['atomic number'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic number", "@{atomic number}"),
        ("Type", "@metal"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic mass' :
        color_mapper = linear_cmap(field_name = 'atomic mass', palette = Colorblind[8], 
                                   low = dffa['atomic mass'].min(), 
                                   high = dffa['atomic mass'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Atomic mass", "@{atomic mass} amu"),
        ("Type", "@metal")
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_hfomo' :
        color_mapper = linear_cmap(field_name = 'atomic_hfomo', palette = Colorblind[8], 
                                   low = dffa['atomic_hfomo'].min(), 
                                   high = dffa['atomic_hfomo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hfomo", "@atomic_hfomo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
    
    if Feature == 'atomic_hpomo' :
        color_mapper = linear_cmap(field_name = 'atomic_hpomo', palette = Colorblind[8], 
                                   low = dffa['atomic_hpomo'].min(), 
                                   high = dffa['atomic_hpomo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_hpomo", "@atomic_hpomo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_lfumo' :
        color_mapper = linear_cmap(field_name = 'atomic_lfumo', palette = Colorblind[8], 
                                   low = dffa['atomic_lfumo'].min(), 
                                   high = dffa['atomic_lfumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lfumo", "@atomic_lfumo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_hpumo' :
        color_mapper = linear_cmap(field_name = 'atomic_hpumo', palette = Colorblind[8], 
                                   low = dffa['atomic_hpumo'].min(), 
                                   high = dffa['atomic_hpumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lpumo", "@atomic_hpumo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_lpumo' :
        color_mapper = linear_cmap(field_name = 'atomic_lpumo', palette = Colorblind[8], 
                                   low = dffa['atomic_lpumo'].min(), 
                                   high = dffa['atomic_lpumo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_lpumo", "@atomic_lpumo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_ip_by_half_charged_homo' :
        color_mapper = linear_cmap(field_name = 'atomic_ip_by_half_charged_homo', palette = Colorblind[8], 
                                   low = dffa['atomic_ip_by_half_charged_homo'].min(), 
                                   high = dffa['atomic_ip_by_half_charged_homo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ip_by_half_charged_homo", "@atomic_ip_by_half_charged_homo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    if Feature == 'atomic_ea_by_half_charged_homo' :
        color_mapper = linear_cmap(field_name = 'atomic_ea_by_half_charged_homo', palette = Colorblind[8], 
                                   low = dffa['atomic_ea_by_half_charged_homo'].min(), 
                                   high = dffa['atomic_ea_by_half_charged_homo'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_ea_by_half_charged_homo", "@atomic_ea_by_half_charged_homo eV"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
    #non spin features
        
    if Feature == 'atomic_r_s_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_s_neg_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_s_neg_1'].min(), 
                                   high = dffa['atomic_r_s_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_s_neg_1", "@atomic_r_s_neg_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_p_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_p_neg_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_p_neg_1'].min(), 
                                   high = dffa['atomic_r_p_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_p_neg_1", "@atomic_r_p_neg_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_d_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_d_neg_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_d_neg_1'].min(), 
                                   high = dffa['atomic_r_d_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_d_neg_1", "@atomic_r_d_neg_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_val_neg_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_val_neg_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_val_neg_1'].min(), 
                                   high = dffa['atomic_r_val_neg_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_val_neg_1", "@atomic_r_val_neg_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_s_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_s_neg_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_s_neg_05'].min(), 
                                   high = dffa['atomic_r_s_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_s_neg_05", "@atomic_r_s_neg_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_p_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_p_neg_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_p_neg_05'].min(), 
                                   high = dffa['atomic_r_p_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_p_neg_05", "@atomic_r_p_neg_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_d_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_d_neg_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_d_neg_05'].min(), 
                                   high = dffa['atomic_r_d_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_d_neg_05", "@atomic_r_d_neg_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_val_neg_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_val_neg_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_val_neg_05'].min(), 
                                   high = dffa['atomic_r_val_neg_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_val_neg_05", "@atomic_r_val_neg_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_s' :
        color_mapper = linear_cmap(field_name = 'atomic_r_s', palette = Colorblind[8], 
                                   low = dffa['atomic_r_s'].min(), 
                                   high = dffa['atomic_r_s'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_s", "@atomic_r_s pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_p' :
        color_mapper = linear_cmap(field_name = 'atomic_r_p', palette = Colorblind[8], 
                                   low = dffa['atomic_r_p'].min(), 
                                   high = dffa['atomic_r_p'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_p", "@atomic_r_p pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_d' :
        color_mapper = linear_cmap(field_name = 'atomic_r_d', palette = Colorblind[8], 
                                   low = dffa['atomic_r_d'].min(), 
                                   high = dffa['atomic_r_d'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_d", "@atomic_r_d pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_val' :
        color_mapper = linear_cmap(field_name = 'atomic_r_val', palette = Colorblind[8], 
                                   low = dffa['atomic_r_val'].min(), 
                                   high = dffa['atomic_r_val'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_val", "@atomic_r_val pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_s_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_s_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_s_05'].min(), 
                                   high = dffa['atomic_r_s_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_s_05", "@atomic_r_s_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_p_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_p_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_p_05'].min(), 
                                   high = dffa['atomic_r_p_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_p_05", "@atomic_r_p_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_d_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_d_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_d_05'].min(), 
                                   high = dffa['atomic_r_d_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_d_05", "@atomic_r_d_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_val_05' :
        color_mapper = linear_cmap(field_name = 'atomic_r_val_05', palette = Colorblind[8], 
                                   low = dffa['atomic_r_val_05'].min(), 
                                   high = dffa['atomic_r_val_05'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_val_05", "@atomic_r_val_05 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_s_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_s_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_s_1'].min(), 
                                   high = dffa['atomic_r_s_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_s_1", "@atomic_r_s_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_p_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_p_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_p_1'].min(), 
                                   high = dffa['atomic_r_p_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_p_1", "@atomic_r_p_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_d_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_d_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_d_1'].min(), 
                                   high = dffa['atomic_r_d_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_d_1", "@atomic_r_d_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
    if Feature == 'atomic_r_val_1' :
        color_mapper = linear_cmap(field_name = 'atomic_r_val_1', palette = Colorblind[8], 
                                   low = dffa['atomic_r_val_1'].min(), 
                                   high = dffa['atomic_r_val_1'].max())
        p2.hover.tooltips = [
        ("Name", "@name"),
        ("Type", "@metal"),
        ("atomic_r_val_1", "@atomic_r_val_1 pm"),
        ]
        #p2.hover.muted_policy = 'ignore'
        
        

 #   p2 = figure(plot_width=1000, plot_height=550,title="Periodic Table",
   #        x_range=groups, y_range=list(reversed(periods)), toolbar_location=None,tools="hover")
    
    
    
    
    p2.rect("group", "period", 0.90, 0.90, source= source, fill_alpha=0.6,color=color_mapper)
    text_props = {"source":source, "text_align": "left", "text_baseline": "middle", "text_color" : "black"}

    x1 = dodge("group", -0.4, range=p2.x_range)

    r1 = p2.text(x=x1, y="period", text="symbol", **text_props)
    r1.glyph.text_font_style="bold"

    r1 = p2.text(x=x1, y=dodge("period", 0.3, range=p2.y_range), text="atomic number", **text_props)
    r1.glyph.text_font_size="8pt"

    r1 = p2.text(x=x1, y=dodge("period", -0.35, range=p2.y_range), text="name", **text_props)
    r1.glyph.text_font_size="5pt"
    #Defines color bar
    color_bar = ColorBar(color_mapper=color_mapper['transform'],
                         formatter = NumeralTickFormatter(format='0.0000'),label_standoff = 13, width=8, location=(0,0))
    # Set color_bar location
    p2.add_layout(color_bar, 'right')
    p2.outline_line_color = None
    p2.grid.grid_line_color = None
    p2.axis.axis_line_color = None
    p2.axis.major_tick_line_color = None
    p2.axis.major_label_standoff = 0
    p2.axis.visible = False
    output_notebook(hide_banner=True)
    show(p2,notebook_handle=True)
    push_notebook()


# In[7]:


def showfigf():
    return interact(ptableplotgenf, Feature = Feature)

def showfigt():
    return interact(ptableplotgent, Feature = Feature)


# In[8]:



def f(df3,df4):
        for i in df4.atomic_element_symbol:
            for j in df3.symbol:
        #print("In for",i)
                if (i==j):
        #print("In if",i)
                    
                    df3["atomic_hfomo"] = df4["atomic_hfomo"]*6.242e+18
                    df3["atomic_hpomo"] = df4["atomic_hpomo"]*6.242e+18
                    df3["atomic_lfumo"] = df4["atomic_lfumo"]*6.242e+18
                    df3["atomic_lpumo"] = df4["atomic_lpumo"]*6.242e+18
                    df3["atomic_ip_by_half_charged_homo"] = df4["atomic_ip_by_half_charged_homo"]*6.242e+18
                    df3["atomic_ea_by_half_charged_homo"] = df4["atomic_ea_by_half_charged_homo"]*6.242e+18
                    df3["atomic_ip"] = df4["atomic_ip_by_energy_difference"]*6.242e+18
                    df3["atomic_ea"] = df4["atomic_ea_by_energy_difference"]*6.242e+18
                    df3["atomic_r_s_neg_1"] = df4["r_s_neg_1.0"]*100
                    df3["atomic_r_p_neg_1"] = df4["r_p_neg_1.0"]*100
                    df3["atomic_r_d_neg_1"] = df4["r_d_neg_1.0"]*100
                    df3["atomic_r_val_neg_1"] = df4["r_val_neg_1.0"]*100
                    df3["atomic_r_s_neg_05"] = df4["r_s_neg_0.5"]*100
                    df3["atomic_r_p_neg_05"] = df4["r_p_neg_0.5"]*100
                    df3["atomic_r_d_neg_05"] = df4["r_d_neg_0.5"]*100
                    df3["atomic_r_val_neg_05"] = df4["r_val_neg_0.5"]*100
                    df3["atomic_r_s"] = df4["r_s_0.0"]*100
                    df3["atomic_r_p"] = df4["r_p_0.0"]*100
                    df3["atomic_r_d"] = df4["r_d_0.0"]*100
                    df3["atomic_r_val"] = df4["r_val_0.0"]*100
                    df3["atomic_r_s_05"] = df4["r_s_0.5"]*100
                    df3["atomic_r_p_05"] = df4["r_p_0.5"]*100
                    df3["atomic_r_d_05"] = df4["r_d_0.5"]*100
                    df3["atomic_r_val_05"] = df4["r_val_0.5"]*100
                    df3["atomic_r_s_1"] = df4["r_s_1.0"]*100
                    df3["atomic_r_p_1"] = df4["r_p_1.0"]*100
                    df3["atomic_r_d_1"] = df4["r_d_1.0"]*100
                    df3["atomic_r_val_1"] = df4["r_val_1.0"]*100
                    globals()['dffa'] = df3
                
def t(df3,df4):
        for i in df4.atomic_element_symbol:
            for j in df3.symbol:
                if (i==j):
                    df3["atomic_hfomo"] = df4["atomic_hfomo"]*6.242e+18
                    df3["atomic_hpomo"] = df4["atomic_hpomo"]*6.242e+18
                    df3["atomic_lfumo"] = df4["atomic_lfumo"]*6.242e+18
                    df3["atomic_lpumo"] = df4["atomic_lpumo"]*6.242e+18
                    df3["atomic_ip_by_half_charged_homo"] = df4["atomic_ip_by_half_charged_homo"]*6.242e+18
                    df3["atomic_ea_by_half_charged_homo"] = df4["atomic_ea_by_half_charged_homo"]*6.242e+18
                    df3["atomic_ip"] = df4["atomic_ip_by_energy_difference"]*6.242e+18
                    df3["atomic_ea"] = df4["atomic_ea_by_energy_difference"]*6.242e+18
                    df3["atomic_r_up_s_neg_1"] = df4["r_up_s_neg_1.0"]*100
                    df3["atomic_r_dn_s_neg_1"] = df4["r_dn_s_neg_1.0"]*100
                    df3["atomic_r_up_p_neg_1"] = df4["r_up_p_neg_1.0"]*100
                    df3["atomic_r_dn_p_neg_1"] = df4["r_dn_p_neg_1.0"]*100
                    df3["atomic_r_dn_d_neg_1"] = df4["r_dn_d_neg_1.0"]*100
                    df3["atomic_r_dn_d_neg_1"] = df4["r_dn_d_neg_1.0"]*100
                    df3["atomic_r_up_val_neg_1"] = df4["r_up_val_neg_1.0"]*100
                    df3["atomic_r_dn_val_neg_1"] = df4["r_dn_val_neg_1.0"]*100
                    df3["atomic_r_up_s_neg_05"] = df4["r_up_s_neg_0.5"]*100
                    df3["atomic_r_dn_s_neg_05"] = df4["r_dn_s_neg_0.5"]*100
                    df3["atomic_r_up_p_neg_05"] = df4["r_up_p_neg_0.5"]*100
                    df3["atomic_r_dn_p_neg_05"] = df4["r_dn_p_neg_0.5"]*100
                    df3["atomic_r_up_d_neg_05"] = df4["r_up_d_neg_0.5"]*100
                    df3["atomic_r_dn_d_neg_05"] = df4["r_dn_d_neg_0.5"]*100
                    df3["atomic_r_up_val_neg_05"] = df4["r_up_val_neg_0.5"]*100
                    df3["atomic_r_dn_val_neg_05"] = df4["r_dn_val_neg_0.5"]*100
                    df3["atomic_r_up_s"] = df4["r_up_s_0.0"]*100
                    df3["atomic_r_dn_s"] = df4["r_dn_s_0.0"]*100
                    df3["atomic_r_up_p"] = df4["r_up_p_0.0"]*100
                    df3["atomic_r_dn_p"] = df4["r_dn_p_0.0"]*100
                    df3["atomic_r_up_d"] = df4["r_up_d_0.0"]*100
                    df3["atomic_r_dn_d"] = df4["r_dn_d_0.0"]*100
                    df3["atomic_r_up_val"] = df4["r_up_val_0.0"]*100
                    df3["atomic_r_dn_val"] = df4["r_dn_val_0.0"]*100
                    df3["atomic_r_up_s_05"] = df4["r_up_s_0.5"]*100
                    df3["atomic_r_dn_s_05"] = df4["r_dn_s_0.5"]*100
                    df3["atomic_r_up_p_05"] = df4["r_up_p_0.5"]*100
                    df3["atomic_r_dn_p_05"] = df4["r_dn_p_0.5"]*100
                    df3["atomic_r_up_d_05"] = df4["r_up_d_0.5"]*100
                    df3["atomic_r_dn_d_05"] = df4["r_dn_d_0.5"]*100
                    df3["atomic_r_up_val_05"] = df4["r_up_val_0.5"]*100
                    df3["atomic_r_dn_val_05"] = df4["r_dn_val_0.5"]*100
                    df3["atomic_r_up_s_1"] = df4["r_up_s_1.0"]*100
                    df3["atomic_r_dn_s_1"] = df4["r_dn_s_1.0"]*100
                    df3["atomic_r_up_p_1"] = df4["r_up_p_1.0"]*100
                    df3["atomic_r_dn_p_1"] = df4["r_dn_p_1.0"]*100
                    df3["atomic_r_up_d_1"] = df4["r_up_d_1.0"]*100
                    df3["atomic_r_dn_d_1"] = df4["r_dn_d_1.0"]*100
                    df3["atomic_r_up_val_1"] = df4["r_up_val_1.0"]*100
                    df3["atomic_r_dn_val_1"] = df4["r_dn_val_1.0"]*100
                    globals()['dftr'] = df3


# In[9]:


def heatmap(Spin = '', method = ''):
        if Spin.lower() == 'false' and method.lower() == 'hse06':
            clean();
            df4 = pd.read_csv('./data/csv/no_spin_hse06_really_tight.csv')
            f(df3,df4)
            featuref()
            showfigf()
        elif Spin.lower() == 'false' and method.lower() =='pbe':
            clean();
            df4 = pd.read_csv('./data/csv/no_spin_pbe_really_tight.csv')
            f(df3,df4)
            featuref()
            showfigf()
        elif Spin.lower() == 'false' and method.lower() =='pbe0':
            clean();
            df4 = pd.read_csv('./query_nomad_archive/csv/no_spin_pbe0_really_tight.csv')
            f(df3,df4)
            featuref()
            showfigf()
        elif Spin.lower() == 'false' and method.lower() =='pbesol':
            clean();
            df4 = pd.read_csv('./data/csv/no_spin_pbesol_really_tight.csv')
            f(df3,df4)
            featuref()
            showfigf()
        elif Spin.lower() == 'false' and method.lower() =='pw-lda':
            clean();
            df4 = pd.read_csv('./data/csv/no_spin_pw-lda_really_tight.csv')
            f(df3,df4)
            featuref()
            showfigf()
        elif Spin.lower() == 'false' and method.lower() =='revpbe':
            clean();
            df4 = pd.read_csv('./data/csv/no_spin_revpbe_really_tight.csv')
            f(df3,df4)
            featuref()
            showfigf()
        elif Spin.lower() == 'true' and method.lower() =='hse06':
            clean();
            df4 = pd.read_csv('./data/csv/spin_hse06_really_tight.csv')
            t(df3,df4)
            featuret()
            showfigt()
        elif Spin.lower() == 'true' and method.lower() =='pbe':
            clean();
            df4 = pd.read_csv('./data/csv/spin_pbe_really_tight.csv')
            t(df3,df4)
            featuret()
            showfigt()
        elif Spin.lower() == 'true' and method.lower() =='pbe0':
            clean();
            df4 = pd.read_csv('./query_nomad_archive/csv/spin_pbe0_really_tight.csv')
            t(df3,df4)
            featuret()
            showfigt()
        elif Spin.lower() == 'true' and method.lower() =='pbesol':
            clean();
            df4 = pd.read_csv('./data/csv/spin_pbesol_really_tight.csv')
            t(df3,df4)
            featuret()
            showfigt()
        elif Spin.lower() == 'true' and method.lower() =='pw-lda':
            clean();
            df4 = pd.read_csv('./data/csv/spin_pw-lda_really_tight.csv')
            t(df3,df4)
            featuret()
            showfigt()
        elif Spin.lower() == 'true' and method.lower() =='revpbe':
            clean();
            df4 = pd.read_csv('./data/csv/spin_revpbe_really_tight.csv')
            t(df3,df4)
            featuret()
            showfigt()
        else:
            raise Exception("Please check the input parameters or Data for specified functional and spin setting is not available")


# In[ ]:





# In[ ]:




