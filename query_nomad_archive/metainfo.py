#!/usr/bin/env python
# coding: utf-8

# In[1]:


from nomad.metainfo import MSection, Quantity, SubSection
import typing


# ## Nomad-metainfo section definitions

# ### *class metadata: This section contains metadata information of source,method and functional used for atomic features. And is used as subection inside Class atomic_properties_fhi section*

# In[ ]:


class metadata(MSection):
    '''
    This section contains metadata information of source,method and functional used for atomic features.
    
    '''
    source = Quantity(type = str, description='''Provides the name of source 
    of particular atomic feature''')
    atomic_method = Quantity(type = str, description='''Provides the name of functional used for calculation 
    of particular atomic feature''')
    atomic_basis_set= Quantity(type = str, description='''Provides the unique name of basis set applied for computation''')
    atomic_spin_setting = Quantity(type = str, description='''Provides information whether spin settings are turned on or off''')


# In[ ]:



class atomic_properties_fhi(MSection):
    section_fhi_metadata = SubSection(sub_section=metadata)
    #section_FHI_atomicdata = SubSection(sub_section=AtomicData)
    '''
    This section contains metadata information of several atomic features accessible from FHI as source  
    '''
    atomic_number = Quantity(type = int, description='''Provides the number of protons found in nucleus''')
    atomic_element_symbol = Quantity(type = str, description='''Provides the symbol of element as per periodic table''')
    atomic_r_s = Quantity(type = float, description='''Provides the s orbital atomic radii''',shape=['0..*'])
    atomic_r_p = Quantity(type = float, description='''Provides the p orbital atomic radii''',shape=['0..*'])
    atomic_r_d = Quantity(type = float, description='''Provides the d orbital atomic radii''',shape=['0..*'])
    atomic_r_val = Quantity(type = float, description='''Provides the atomic radii of element''',shape=['0..*'])
    atomic_ea = Quantity(type = float, description='''Provides the atomic electron affinity calculated 
    from energy difference''')
    atomic_ip = Quantity(type = float, description='''Provides the atomic ionization potential calculated  from energy difference''')
    atomic_hfomo = Quantity(type = float, description='''Provides the highest dist. of fully filled molecular orbital''')
    atomic_hpomo = Quantity(type = float, description='''Provides the highest dist. of partially filled molecular 
    orbital''')
    atomic_lfumo = Quantity(type = float, description='''Provides the lowest dist. of fully filled molecular 
    orbital''')
    atomic_lpumo = Quantity(type = float, description='''Provides the the lowest dist. of partially filled molecular 
    orbital''')
    atomic_ea_by_half_charged_homo = Quantity(type = float, description='''Provides the half charged atomic electron affinity 
    of HOMO''')
    atomic_ip_by_half_charged_homo = Quantity(type = float, description='''Provides the half charged atomic ionization potential 
    of HOMO''')
    atomic_r_s_neg_1 = Quantity(type = float, description='''Provides the s orbital atomic radii of -1 charged''',shape=['0..*'])
    atomic_r_p_neg_1 = Quantity(type = float, description='''Provides the p orbital atomic radii of -1 charged''',shape=['0..*'])
    atomic_r_d_neg_1 = Quantity(type = float, description='''Provides the d orbital atomic radii of -1 charged''',shape=['0..*'])
    atomic_r_val_neg_1 = Quantity(type = float, description='''Provides the atomic radii of -1 charged''',shape=['0..*'])
    atomic_r_s_neg_05 = Quantity(type = float, description='''Provides the s orbital atomic radii of -0.5 charged''',shape=['0..*'])
    atomic_r_p_neg_05 = Quantity(type = float, description='''Provides the p orbital atomic radii of -0.5 charged''',shape=['0..*'])
    atomic_r_d_neg_05 = Quantity(type = float, description='''Provides the d orbital atomic radii of -0.5 charged''',shape=['0..*'])
    atomic_r_val_neg_05 = Quantity(type = float, description='''Provides the atomic radii of -0.5 charged''',shape=['0..*'])
    atomic_r_s_05 = Quantity(type = float, description='''Provides the s orbital atomic radii of +0.5 charged''',shape=['0..*'])
    atomic_r_p_05 = Quantity(type = float, description='''Provides the p orbital atomic radii of +0.5 charged''',shape=['0..*'])
    atomic_r_d_05 = Quantity(type = float, description='''Provides the d orbital atomic radii of +0.5 charged''',shape=['0..*'])
    atomic_r_val_05 = Quantity(type = float, description='''Provides the atomic radii of +0.5 charged''',shape=['0..*'])
    atomic_r_s_1 = Quantity(type = float, description='''Provides the s orbital atomic radii of +1 charged''',shape=['0..*'])
    atomic_r_p_1 = Quantity(type = float, description='''Provides the p orbital atomic radii of +1 charged''',shape=['0..*'])
    atomic_r_d_1 = Quantity(type = float, description='''Provides the d orbital atomic radii of +1 charged''',shape=['0..*'])
    atomic_r_val_1 = Quantity(type = float, description='''Provides the atomic radii of +1 charged''',shape=['0..*'])


# In[ ]:



class atomic_properties_pymat(MSection):
    section_pymat_metadata = SubSection(sub_section=metadata)
    #section_FHI_atomicdata = SubSection(sub_section=AtomicData)
    '''
    This section contains metadata information of several atomic features accessible from FHI as source  
    '''
    atomic_number = Quantity(type = int, description='''Provides the number of protons found in nucleus''')
    atomic_element_symbol = Quantity(type = str, description='''Provides the symbol of element as per periodic table''')
    atomic_mass = Quantity(type = float, description='''Provides atomic mass for the element''')
    atomic_radius = Quantity(type = float, description='''Provides atomic radius for the element : Angs Units''')
    atomic_radius_calculated = Quantity(type = float, description='''Provides Calculated atomic radius for the element 
    : Angs Units''')
    atomic_orbitals = Quantity(type = typing.Any,description='''Provides energy of the atomic orbitals as a dict : eV Unit''')
    boiling_point = Quantity(type = float, description='''Provides Boiling point to selected element : K Units''')
    brinell_hardness = Quantity(type = float, description='''Provides Brinell Hardness of selected element : [MN]/[m^2] unit''')
    bulk_modulus = Quantity(type = typing.Any, description='''Provides Bulk modulus of selected element : GPa Unit''')
    coeff_olte = Quantity(type = float, description='''Provides Coefficient of linear thermal expansion: [x10^-6]/[K] Unit ''')
    common_ox_states = Quantity(type = typing.Any, description='''Provides the list of common oxidation states in which the 
    element is found''')
    critical_temperature = Quantity(type = float, description='''Provides the Critical temperature of element : K Units''')
    density_of_solid = Quantity(type = float, description='''Provides the density of solid phase : Units [Kg]/[m^3]''')
    electrical_resistivity = Quantity(type = typing.Any, description='''Provides the electrical resistivity of element : [10^-8] 
    Omega.m Units''')
    electronic_structure = Quantity(type = typing.Any, description='''Provides the Electronic structure as string, with only valence electrons''')
    ionic_radii = Quantity(type = typing.Any, description='''Provides All ionic radii of the element as a dict of
            {oxidation state: ionic radii}. Radii are given in ang. ''')
    liquid_range = Quantity(type = float, description='''Provides the liquid range temp. of the element : K units ''')
    melting_point = Quantity(type = typing.Any, description='''Provides the Melting point of the element : K units''')
    mendeleev_no = Quantity(type = float, description='''Provides the Mendeleev number from definition given by 
    Pettifor, D. G. (1984). A chemical scale for crystal-structure maps. Solid State Communications, 51 (1), 31-34''')
    mineral_hardness = Quantity(type = typing.Any, description='''Provides the Mineral Hardness of the element ''')
    molar_volume = Quantity(type = float, description='''Provides the Molar volume of the element : cm^3 Units ''')
    atomic_element_name = Quantity(type = str, description='''Provides name of the full long name of the element''')
    oxidation_states = Quantity(type = typing.Any, description='''Provides the python list of all known oxidation states''')
    poisson_ratio = Quantity(type = float, description='''Provides the Poisson's ratio of the element''')
    atomic_refelctivity = Quantity(type = float, description='''Provides the Reflectivity of the element : % Unit''')
    atomic_refractive_index = Quantity(type = typing.Any, description='''Provides the Refractice index of the element''')
    rigidity_modulus = Quantity(type = float, description='''Provides the  Rigidity modulus of the element : GPa Units''')
    shannon_radii = Quantity(type = typing.Any, description='''Provides the (python) dictionary (key-value pairs) Shannon radius for 
    specie in the different environments
    Oxdiation_no : -4...6
    cn: Coordination using roman letters. Values are I-IX, as well as IIIPY, IVPY and IVSQ.
    spin: Some species have different radii for different spins. One can get specific values using "High Spin" or"Low Spin". 
          Leave it as "" if not available. If only one spin data is available, it is returned and this spin parameter is ignored.
    radius_type: Either "crystal" or "ionic"
    typical dict would have key in specific format "Oxdiation_no.cn.spin.crystal_radius"  ''')
    supercond_temp = Quantity(type = typing.Any, description='''Provides the  Superconduction temperature of the element : K Units''')
    thermal_cond = Quantity(type = typing.Any, description='''Provides the Thermal conductivity of the element : W/[mK]''')
    van_der_waals_rad = Quantity(type = float, description='''Provides the Van der Waals radius for the element. This is the 
    empirical value(http://en.wikipedia.org/wiki/Atomic_radii_of_the_elements_(data_page))''')
    velocity_of_sound = Quantity(type = float, description='''Provides the Velocity of sound in the element : m/s Units''')
    vickers_hardness = Quantity(type = float, description='''Provides the Vicker's hardness of the element : MN/[m^-2] Units''')
    x = Quantity(type = float, description='''Provides the Pauling electronegativity. Elements without an electronegativity
    number are assigned a value of zero by default''')
    youngs_modulus = Quantity(type = float, description='''Provides the Young's modulus of the element : GPa Units''')
    metallic_radius = Quantity(type = float, description='''Provides the Metallic radius of the element : Angs Units''')
    iupac_ordering = Quantity(type = float, description='''Ordering according to Table VI of "Nomenclature of Inorganic Chemistry
    (IUPAC Recommendations 2005)". This ordering effectively follows the groups and rows of the periodic table, except the 
    Lanthanides, Actanides and hydrogen''')
    icsd_oxd_states = Quantity(type = typing.Any, description='''Provides list(python) of all oxidation states with 
    at least 10 instances in ICSD database AND at least 1% of entries for that element''')
    nmr_quadrapole_mom = Quantity(type = typing.Any, description='''Provides a dictionary of the nuclear electric 
    quadrupole moment in units of e*millibarns for various isotopes''')
    max_oxd_state = Quantity(type = float, description='''Provides the Maximum oxidation state for element''')
    min_oxd_state = Quantity(type = float, description='''Provides the Minimum oxidation state for element''')
    ionic_radii_hs = Quantity(type = typing.Any, description='''Provides the Ionic radius of specie : Angs Unit''')
    ionic_radii_ls = Quantity(type = typing.Any, description='''Provides the Ionic radius of specie : Angs Unit''')

