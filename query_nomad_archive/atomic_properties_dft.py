#!/usr/bin/env python
# coding: utf-8

# In[1]:


from query_nomad_archive.metainfo import atomic_properties_fhi,metadata
import pandas as pd


# 
# # Section instantiation and its objects attributes are filled  using functions f and t. Each function call will create 102 objects unique ( atomic element symbol) which can be used to access its properties  

# In[2]:


def f(df1,objs):
    for i in df1['atomic_element_symbol']:
        fhi = atomic_properties_fhi()
        prop = fhi.m_create(metadata)
        objs.append(fhi)
    
    count = 0
    for obj in objs:
        obj.section_fhi_metadata.source = 'FHIaims'
        obj.section_fhi_metadata.atomic_spin_setting = 'FALSE'
        obj.atomic_number = int(df1.iloc[count,df1.columns.get_loc("atomic_number")])
        obj.atomic_element_symbol = str(df1.iloc[count,df1.columns.get_loc("atomic_element_symbol")])
        obj.section_fhi_metadata.atomic_method = str(df1.iloc[count,df1.columns.get_loc("atomic_method")])
        obj.section_fhi_metadata.atomic_basis_set = str(df1.iloc[count,df1.columns.get_loc("atomic_basis_set")])
        obj.atomic_hfomo = float(df1.iloc[count,df1.columns.get_loc("atomic_hfomo")])
        obj.atomic_hpomo = float(df1.iloc[count,df1.columns.get_loc("atomic_hpomo")])
        obj.atomic_lfumo = float(df1.iloc[count,df1.columns.get_loc("atomic_lfumo")])
        obj.atomic_lpumo = float(df1.iloc[count,df1.columns.get_loc("atomic_lpumo")])
        obj.atomic_ea = float(df1.iloc[count,df1.columns.get_loc("atomic_ea_by_energy_difference")])
        obj.atomic_ip = float(df1.iloc[count,df1.columns.get_loc("atomic_ip_by_energy_difference")])
        obj.atomic_ea_by_half_charged_homo = float(df1.iloc[count,df1.columns.get_loc("atomic_ea_by_half_charged_homo")])
        obj.atomic_ip_by_half_charged_homo = float(df1.iloc[count,df1.columns.get_loc("atomic_ip_by_half_charged_homo")])
        obj.atomic_r_s_neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_s_neg_1.0")])]
        obj.atomic_r_p_neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_p_neg_1.0")])]
        obj.atomic_r_d__neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_d_neg_1.0")])]
        obj.atomic_r_val_neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_val_neg_1.0")])]
        obj.atomic_r_s_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_s_neg_0.5")])]
        obj.atomic_r_p_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_p_neg_0.5")])]
        obj.atomic_r_d_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_d_neg_0.5")])]
        obj.atomic_r_val_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_val_neg_0.5")])]
        obj.atomic_r_s = [float(df1.iloc[count,df1.columns.get_loc("r_s_0.0")])]
        obj.atomic_r_p = [float(df1.iloc[count,df1.columns.get_loc("r_p_0.0")])]
        obj.atomic_r_d = [float(df1.iloc[count,df1.columns.get_loc("r_d_0.0")])]
        obj.atomic_r_val = [float(df1.iloc[count,df1.columns.get_loc("r_val_0.0")])]
        obj.atomic_r_s_05 = [float(df1.iloc[count,df1.columns.get_loc("r_s_0.5")])]
        obj.atomic_r_p_05 = [float(df1.iloc[count,df1.columns.get_loc("r_p_0.5")])]
        obj.atomic_r_d_05 = [float(df1.iloc[count,df1.columns.get_loc("r_d_0.5")])]
        obj.atomic_r_val_05 = [float(df1.iloc[count,df1.columns.get_loc("r_val_0.5")])]
        obj.atomic_r_s_1 = [float(df1.iloc[count,df1.columns.get_loc("r_s_1.0")])]
        obj.atomic_r_p_1 = [float(df1.iloc[count,df1.columns.get_loc("r_p_1.0")])]
        obj.atomic_r_d_1 = [float(df1.iloc[count,df1.columns.get_loc("r_d_1.0")])]
        obj.atomic_r_val_1 =[float(df1.iloc[count,df1.columns.get_loc("r_val_1.0")])] 
        count+=1
        
    atomic_element_symbol = df1['atomic_element_symbol'].values
    for count, symbol in enumerate(atomic_element_symbol):
        globals()[symbol] = objs[count]
                
        
def t(df1,objs):
    for i in df1['atomic_element_symbol']:
        fhi = atomic_properties_fhi()
        prop = fhi.m_create(metadata)
        objs.append(fhi)
        
    count = 0
    for obj in objs:
        obj.section_fhi_metadata.source = 'FHIaims'
        obj.section_fhi_metadata.atomic_spin_setting = 'TRUE'
        obj.atomic_number = int(df1.iloc[count,df1.columns.get_loc("atomic_number")])
        obj.atomic_element_symbol = str(df1.iloc[count,df1.columns.get_loc("atomic_element_symbol")])
        obj.section_fhi_metadata.atomic_method = str(df1.iloc[count,df1.columns.get_loc("atomic_method")])
        obj.section_fhi_metadata.atomic_basis_set = str(df1.iloc[count,df1.columns.get_loc("atomic_basis_set")])
        obj.atomic_hfomo = float(df1.iloc[count,df1.columns.get_loc("atomic_hfomo")])
        obj.atomic_hpomo = float(df1.iloc[count,df1.columns.get_loc("atomic_hpomo")])
        obj.atomic_lfumo = float(df1.iloc[count,df1.columns.get_loc("atomic_lfumo")])
        obj.atomic_lpumo = float(df1.iloc[count,df1.columns.get_loc("atomic_lpumo")])
        obj.atomic_ea = float(df1.iloc[count,df1.columns.get_loc("atomic_ea_by_energy_difference")])
        obj.atomic_ip = float(df1.iloc[count,df1.columns.get_loc("atomic_ip_by_energy_difference")])
        obj.atomic_ea_by_half_charged_homo = float(df1.iloc[count,df1.columns.get_loc("atomic_ea_by_half_charged_homo")])
        obj.atomic_ip_by_half_charged_homo = float(df1.iloc[count,df1.columns.get_loc("atomic_ip_by_half_charged_homo")])
        obj.atomic_r_s_neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_s_neg_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_s_neg_1.0")])]
        obj.atomic_r_p_neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_p_neg_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_p_neg_1.0")])]
        obj.atomic_r_d__neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_dn_d_neg_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_d_neg_1.0")])]
        obj.atomic_r_val_neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_val_neg_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_val_neg_1.0")])]
        obj.atomic_r_s_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_s_neg_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_s_neg_0.5")])]
        obj.atomic_r_p_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_p_neg_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_p_neg_0.5")])]
        obj.atomic_r_d_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_d_neg_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_d_neg_0.5")])]
        obj.atomic_r_val_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_val_neg_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_val_neg_0.5")])]
        obj.atomic_r_s = [float(df1.iloc[count,df1.columns.get_loc("r_up_s_0.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_s_0.0")])]
        obj.atomic_r_p = [float(df1.iloc[count,df1.columns.get_loc("r_up_p_0.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_p_0.0")])]
        obj.atomic_r_d = [float(df1.iloc[count,df1.columns.get_loc("r_up_d_0.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_d_0.0")])]
        obj.atomic_r_val = [float(df1.iloc[count,df1.columns.get_loc("r_up_val_0.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_val_0.0")])]
        obj.atomic_r_s_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_s_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_s_0.5")])]
        obj.atomic_r_p_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_p_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_p_0.5")])]
        obj.atomic_r_d_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_d_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_d_0.5")])]
        obj.atomic_r_val_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_val_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_val_0.5")])]
        obj.atomic_r_s_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_s_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_s_1.0")])]
        obj.atomic_r_p_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_p_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_p_1.0")])]
        obj.atomic_r_d_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_d_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_d_1.0")])]
        obj.atomic_r_val_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_val_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_val_1.0")])]
        count+=1
        
    atomic_element_symbol = df1['atomic_element_symbol'].values
    for count, symbol in enumerate(atomic_element_symbol):
        globals()[symbol] = objs[count]


# 
# # This function one has to call with it params as per ones interest and method  

# In[3]:


def method(Spin = '', method = ''):
    if Spin.lower() == 'false' and method.lower() == 'hse06':
        df1 = pd.read_csv('./data/csv/no_spin_hse06_really_tight.csv')
        objs = []
        f(df1,objs)
    elif Spin.lower() == 'false' and method.lower() =='pbe':
        df1 = pd.read_csv('./data/csv/no_spin_pbe_really_tight.csv')
        objs = []
        f(df1,objs)
    elif Spin.lower() == 'false' and method.lower() =='pbe0':
        df1 = pd.read_csv('./data/csv/no_spin_pbe0_really_tight.csv')
        objs = []
        f(df1,objs)
    elif Spin.lower() == 'false' and method.lower() =='pbesol':
        df1 = pd.read_csv('./data/csv/no_spin_pbesol_really_tight.csv')
        objs = []
        f(df1,objs)
    elif Spin.lower() == 'false' and method.lower() =='pw-lda':
        df1 = pd.read_csv('./data/csv/no_spin_pw-lda_really_tight.csv')
        objs = []
        f(df1,objs)
    elif Spin.lower() == 'false' and method.lower() =='revpbe':
        df1 = pd.read_csv('./data/csv/no_spin_revpbe_really_tight.csv')
        objs = []
        f(df1,objs)
    elif Spin.lower() == 'true' and method.lower() =='hse06':
        df1 = pd.read_csv('./data/csv/spin_hse06_really_tight.csv')
        objs = []
        t(df1,objs)
    elif Spin.lower() == 'true' and method.lower() =='pbe':
        df1 = pd.read_csv('./data/csv/spin_pbe_really_tight.csv')
        objs = []
        t(df1,objs)
    elif Spin.lower() == 'true' and method.lower() =='pbe0':
        df1 = pd.read_csv('./data/csv/spin_pbe0_really_tight.csv')
        objs = []
        t(df1,objs)
    elif Spin.lower() == 'true' and method.lower() =='pbesol':
        df1 = pd.read_csv('./data/csv/spin_pbesol_really_tight.csv')
        objs = []
        t(df1,objs)
    elif Spin.lower() == 'true' and method.lower() =='pw-lda':
        df1 = pd.read_csv('./data/csv/spin_pw-lda_really_tight.csv')
        objs = []
        t(df1,objs)
    elif Spin.lower() == 'true' and method.lower() =='revpbe':
        df1 = pd.read_csv('./data/csv/spin_revpbe_really_tight.csv')
        objs = []
        t(df1,objs)
    else:
        raise Exception("Please check the input parameters or Data for specified functional and spin setting is not available")
       


# ### This function can be called to access quantity definitions

# In[4]:


def definition(abc):
    if abc == 'atomic_number':
        return atomic_properties_fhi.atomic_number.__doc__
    if abc == 'atomic_element_symbol':
        return atomic_properties_fhi.atomic_element_symbol.__doc__
    if abc == 'atomic_r_s':
        return atomic_properties_fhi.atomic_r_s.__doc__
    if abc == 'atomic_r_p':
        return atomic_properties_fhi.atomic_r_p.__doc__
    if abc == 'atomic_r_d':
        return atomic_properties_fhi.atomic_r_d.__doc__
    if abc == 'atomic_r_val':
        return atomic_properties_fhi.atomic_r_val.__doc__
    if abc == 'atomic_ea':
        return atomic_properties_fhi.atomic_ea.__doc__
    if abc == 'atomic_ip':
        return atomic_properties_fhi.atomic_ip.__doc__
    if abc == 'atomic_hpomo':
        return atomic_properties_fhi.atomic_hpomo.__doc__
    if abc == 'atomic_hfomo':
        return atomic_properties_fhi.atomic_hfomo.__doc__
    if abc == 'atomic_lfumo':
        return atomic_properties_fhi.atomic_lfumo.__doc__
    if abc == 'atomic_lpumo':
        return atomic_properties_fhi.atomic_lpumo.__doc__
    if abc == 'atomic_ea_by_energy_difference':
        return atomic_properties_fhi.atomic_ea_by_energy_difference.__doc__
    if abc == 'atomic_ip_by_energy_difference':
        return atomic_properties_fhi.atomic_ip_by_energy_difference.__doc__
    if abc == 'atomic_ea_by_half_charged_homo':
        return atomic_properties_fhi.atomic_ea_by_half_charged_homo.__doc__
    if abc == 'atomic_ip_by_half_charged_homo':
        return atomic_properties_fhi.atomic_ip_by_half_charged_homo.__doc__
    if abc == 'atomic_r_s_neg_1':
        return atomic_properties_fhi.atomic_r_s_neg_1.__doc__
    if abc == 'atomic_r_p_neg_1':
        return atomic_properties_fhi.atomic_r_p_neg_1.__doc__
    if abc == 'atomic_r_d_neg_1':
        return atomic_properties_fhi.atomic_r_d_neg_1.__doc__
    if abc == 'atomic_r_val_neg_1':
        return atomic_properties_fhi.atomic_r_val_neg_1.__doc__
    if abc == 'atomic_r_s_neg_05':
        return atomic_properties_fhi.atomic_r_s_neg_05.__doc__
    if abc == 'atomic_r_p_neg_05':
        return atomic_properties_fhi.atomic_r_p_neg_05.__doc__
    if abc == 'atomic_r_d_neg_05':
        return atomic_properties_fhi.atomic_r_d_neg_05.__doc__
    if abc == 'atomic_r_val_neg_05':
        return atomic_properties_fhi.atomic_r_s_neg_05.__doc__
    if abc == 'atomic_r_s_05':
        return atomic_properties_fhi.atomic_r_s_05.__doc__
    if abc == 'atomic_r_p_05':
        return atomic_properties_fhi.atomic_r_p_05.__doc__
    if abc == 'atomic_r_d_05':
        return atomic_properties_fhi.atomic_r_d_05.__doc__
    if abc == 'atomic_r_val_05':
        return atomic_properties_fhi.atomic_r_val_05.__doc__
    if abc == 'atomic_r_s_1':
        return atomic_properties_fhi.atomic_r_s_1.__doc__
    if abc == 'atomic_r_p_1':
        return atomic_properties_fhi.atomic_r_p_1.__doc__
    if abc == 'atomic_r_d_1':
        return atomic_properties_fhi.atomic_r_d_1.__doc__
    if abc == 'atomic_r_val_1':
        return atomic_properties_fhi.atomic_r_val_1.__doc__
    if abc == 'source':
        return metadata.source.__doc__
    if abc == 'atomic_spin_setting':
        return metadata.atomic_spin_setting.__doc__
    if abc == 'atomic_method':
        return metadata.atomic_method.__doc__
    if abc == 'atomic_basis_set':
        return metadata.atomic_basis_set.__doc__
    
    
    
    
    
    
    


# ### This function utilty is, it can be called to access element properties based on its symbol in periodic table. Can be usefull if want to acess particular property of multiple elements at once

# In[5]:


def symbol(abc):
    return globals()[abc]


# In[ ]:




